package com.learnkafkastreams.serdes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.learnkafkastreams.domain.Greeting;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class GreetingDeserializer implements Deserializer<Greeting> {

    private static final Logger log = LoggerFactory.getLogger(GreetingDeserializer.class);
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Greeting deserialize(String s, byte[] bytes) {
        try {
            return objectMapper.readValue(bytes, Greeting.class);
        } catch (IOException e) {
            log.error("Error deserializing greeting", e);
            throw new RuntimeException(e);
        }
    }
}
