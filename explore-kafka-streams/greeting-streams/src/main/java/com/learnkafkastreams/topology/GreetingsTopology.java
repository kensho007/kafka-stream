package com.learnkafkastreams.topology;

import com.learnkafkastreams.domain.Greeting;
import com.learnkafkastreams.serdes.SerdesFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Printed;
import org.apache.kafka.streams.kstream.Produced;

import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
public class GreetingsTopology {

    public static String GREETINGS = "greetings";

    public static String GREETINGS_UPPERCASE = "greetings_uppercase";
    public static String GREETINGS_SPANISH = "greetings_spanish";

    public static Topology buildTopology() {
        StreamsBuilder streamsBuilder = new StreamsBuilder();

        var mergedStream = getCustomGreetingKStream(streamsBuilder);


        //greetingStream.print(Printed.<String, String>toSysOut().withLabel("greeting streams"));
        //mergedStream.print(Printed.<String, String>toSysOut().withLabel("merged streams"));
        mergedStream.print(Printed.<String, Greeting>toSysOut().withLabel("greeting streams"));

        var modifiedStream = mergedStream
//                .flatMap((key, value) -> {
//                    var newValues = Arrays.asList(value.split(""));
//                    return newValues.stream()
//                            .map(val -> KeyValue.pair(key, val.toUpperCase()))
//                            .collect(Collectors.toList());
//                });
//                .filter((key, value) -> value.length() > 5)
//                .peek((key, value) -> {
//                    log.info("after filter : key {}, value {}", key, value);
//                })
                .mapValues((key, value) ->
                        new Greeting(value.message().toUpperCase(), value.timeStamp()));
                        //value.message().toUpperCase())
               // .peek((key, value) -> {
                //    log.info("after map values : key {}, value {}", key, value);
               // });
        //.map((key, value) -> KeyValue.pair(key.toUpperCase(), value.toUpperCase()));

        modifiedStream.print(Printed.<String, Greeting>toSysOut().withLabel("modified greeting streams"));

        modifiedStream.to(
                GREETINGS_UPPERCASE
                ,Produced.with(Serdes.String(),SerdesFactory.greetingSerde())
                );

        return streamsBuilder.build();
    }

    private static KStream<String, String> getStringGreetingKStream(StreamsBuilder streamsBuilder) {
        KStream<String,String> greetingStream = streamsBuilder.stream(GREETINGS

                //,Consumed.with(Serdes.String(), Serdes.String())
        );

        KStream<String,String> greetingSpanishStream = streamsBuilder.stream(GREETINGS_SPANISH,
                Consumed.with(Serdes.String(), Serdes.String()));

        return greetingStream.merge(greetingSpanishStream);
    }

    private static KStream<String, Greeting> getCustomGreetingKStream(StreamsBuilder streamsBuilder) {
        var greetingStream = streamsBuilder.stream(GREETINGS,
                Consumed.with(Serdes.String(), SerdesFactory.greetingSerde())
        );

        var greetingSpanishStream = streamsBuilder.stream(GREETINGS_SPANISH,
                Consumed.with(Serdes.String(), SerdesFactory.greetingSerde()));

        return greetingStream.merge(greetingSpanishStream);
    }
}
